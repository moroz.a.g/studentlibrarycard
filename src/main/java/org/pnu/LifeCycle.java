/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.pnu;

import io.quarkus.runtime.ShutdownEvent;
import io.quarkus.runtime.StartupEvent;
import jakarta.enterprise.context.ApplicationScoped;
import jakarta.enterprise.event.Observes;
import jakarta.inject.Inject;
import org.jboss.logging.Logger;

/**
 *
 * @author student
 */
@ApplicationScoped
public class LifeCycle {
    private static final Logger log = Logger.getLogger(LifeCycle.class.getName());

    @Inject
    StudentCardService studentcardservice; 
    
    public void start(@Observes StartupEvent event) {
        log.info("Старт сервера ...");
        
        StudentLibraryCard card1 = new StudentLibraryCard();
        StudentLibraryCard card2 = new StudentLibraryCard();
        
        card1.setName("Иван");
        card1.setFirsname("Васильевич");
        card1.setSurname("Иванов");
        card1.setNumberGroup("Пи(б)-11");
        
        card2.setName("Мария");
        card2.setFirsname("Евгеньевна");
        card2.setSurname("Миронова");
        card2.setNumberGroup("Пи(б)-11");
        
        
        studentcardservice.save(card1);
        studentcardservice.save(card2);
       
      
        
        log.info("Старт сервера завершен");
    }

    public void stop(@Observes ShutdownEvent event) {
        log.info("Остановка сервера ...");
        log.info("Остановка сервера завершена");
    }
}
