/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package org.pnu;

import jakarta.enterprise.context.ApplicationScoped;
import jakarta.inject.Inject;
import jakarta.persistence.EntityManager;
import jakarta.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import org.jboss.logging.Logger;

/**
 *
 * @author student
 */
@ApplicationScoped
public class StudentCardService {
    
    private static final Logger log = Logger.getLogger(StudentCardService.class.getName());
    
    @Inject
    EntityManager em;
    
     @Transactional
    public StudentLibraryCard save(StudentLibraryCard s) {
        if (s != null) {
            try {
                em.persist(s);
            } catch (Exception e) {
                System.out.println("Ошибка " + e.getClass().getSimpleName() + " при сохранении планов: " + e.getMessage());
            }
        } else {
            System.out.println("Ошибка сохранения - пришёл пустой объект");
        }
        return s;
    }    
    
//    @Transactional
//    public List<ScienceDTO> getAll() {
//
//        List<Science> scienceList = new ArrayList<>();
//        
//        List<ScienceDTO> scienceListDTO = new ArrayList<>();
//
//        log.info("Получение списка наук ...");
//        scienceList = (List<Science>) em.createQuery("Select s from " + Science.class.getSimpleName() + " s").getResultList();
//        log.info("Получен список наук [" + scienceList.size() + "]");
//        
//        //итерация по записям Science из бд
//        for (Science science : scienceList) {
//            log.info(science.toString());
//            
//            ScienceDTO scienceDTO = new ScienceDTO(science.getId(), science.getName());
//            scienceListDTO.add(scienceDTO);
//            
//            
//            List<ScienceGroupDTO> scienceGroupList = new ArrayList<>();
//            
//            log.info("Количество записей групп наук [" + science.getScienceGroupList().size() + "]");
//            for (ScienceGroup sg : science.getScienceGroupList()){
//                log.info("Группа наук: "+ sg);
//                
//                ScienceGroupDTO scienceGroupDTO = new ScienceGroupDTO(sg.getId(), sg.getName());
//                scienceGroupList.add(scienceGroupDTO);
//                
//                
//            }
//            
//            scienceDTO.setScienceGroupList(scienceGroupList);
//        }
    
}
